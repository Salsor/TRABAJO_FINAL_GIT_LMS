package t12p01;

import javax.swing.JOptionPane;
import modelo.ConexionBD;
import modelo.Curso;

public class PanelCursoBaja extends javax.swing.JPanel {

    private ConexionBD bd;

    public PanelCursoBaja(ConexionBD bd) {
        initComponents();
        this.bd = bd;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        PanelCursoBaja_TextField_id = new javax.swing.JTextField();
        pcb_but_aceptar = new javax.swing.JButton();
        pcb_but_cancelar = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("BAJA DE CURSO");

        jLabel2.setText("Identificador:");

        pcb_but_aceptar.setText("Dar baja");
        pcb_but_aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pcb_but_aceptarActionPerformed(evt);
            }
        });

        pcb_but_cancelar.setText("Cancelar");
        pcb_but_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pcb_but_cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pcb_but_cancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pcb_but_aceptar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(150, 150, 150)
                            .addComponent(jLabel1))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(37, 37, 37)
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(PanelCursoBaja_TextField_id, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(PanelCursoBaja_TextField_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pcb_but_aceptar)
                    .addComponent(pcb_but_cancelar))
                .addContainerGap(110, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pcb_but_aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pcb_but_aceptarActionPerformed
        if (IDvacio()) {
            JOptionPane.showMessageDialog(this, "Hay campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        }
        try {
            Curso c = new Curso();
            String id = PanelCursoBaja_TextField_id.getText();
            c.setId(Integer.parseInt(id));
            c.bajaCurso(bd);
            JOptionPane.showMessageDialog(this, "Baja de curso correcta. ID: " + c.getId(),
                    "Base de datos", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error!!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_pcb_but_aceptarActionPerformed

    public boolean IDvacio() {
        if (PanelCursoBaja_TextField_id.getText().equals("")) {
            return true;
        } else {
            return false;
        }

    }

    private void pcb_but_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pcb_but_cancelarActionPerformed
        this.setVisible(false);
        PanelCursoBaja_TextField_id.setText("");
    }//GEN-LAST:event_pcb_but_cancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField PanelCursoBaja_TextField_id;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton pcb_but_aceptar;
    private javax.swing.JButton pcb_but_cancelar;
    // End of variables declaration//GEN-END:variables
}
