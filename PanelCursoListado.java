package t12p01;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import modelo.ConexionBD;
import modelo.Curso;

public class PanelCursoListado extends javax.swing.JPanel {

    private ConexionBD bd;
    
    public PanelCursoListado(ConexionBD bd) {
        initComponents();
        this.bd=bd;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        pcl_list_lista = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        pcl_but_salir = new javax.swing.JButton();

        jScrollPane1.setViewportView(pcl_list_lista);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("LISTADO DE CURSOS");

        pcl_but_salir.setText("Salir");
        pcl_but_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pcl_but_salirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(138, 138, 138)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pcl_but_salir)
                .addGap(18, 18, 18))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pcl_but_salir)
                .addContainerGap(45, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pcl_but_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pcl_but_salirActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_pcl_but_salirActionPerformed

    public void listadoCurso(DefaultListModel modeloLista)
    {
        modeloLista.removeAllElements();
        try {
            String sql="SELECT * FROM Cursos";
            ResultSet rs=bd.getSt().executeQuery(sql);
            Curso c;
            while (rs.next()) {
                c=new Curso();
                c.setId(rs.getInt("id"));
                c.setTitulo(rs.getString("titulo"));
                c.setHoras(rs.getDouble("horas"));
                modeloLista.addElement("ID: "+c.getId()+"   TITULO: "+c.getTitulo()+"   HORAS: "+c.getHoras());
            }
            pcl_list_lista.setModel(modeloLista);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,
                            "Error!! " + e.getMessage(), "Error",
                            JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton pcl_but_salir;
    private javax.swing.JList<String> pcl_list_lista;
    // End of variables declaration//GEN-END:variables
}
