package t12p01;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import modelo.ConexionBD;
import modelo.Curso;

public class T12p01_GUI extends javax.swing.JFrame {

    private PanelCursoAlta panelCA;
    private PanelCursoBaja panelCB;
    private PanelCursoListado panelCL;
    private ConexionBD bd;
    private DefaultListModel modeloLista;

    public T12p01_GUI() {
        bd = new ConexionBD();
        this.setLocationRelativeTo(null);
        panelCA = new PanelCursoAlta(bd);
        panelCB = new PanelCursoBaja(bd);
        panelCL = new PanelCursoListado(bd);
        modeloLista = new DefaultListModel();
        add(panelCA);
        pack();
        add(panelCB);
        pack();
        add(panelCL);
        pack();
        panelCA.setVisible(false);
        panelCB.setVisible(false);
        panelCL.setVisible(false);
        initComponents();
        /* ABRIR CONEXIÓN BD **************************************************/
        try {
            //System.out.println("Abriendo conexión BD...");
            bd.abrirConexion();
            //System.out.println("Conexión abierta correctamente.");
            JOptionPane.showMessageDialog(this, "Conexion abierta correctamente", "Base de datos", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            //System.out.println("Error!!\n"+e.getMessage());
            JOptionPane.showMessageDialog(this, "Error!!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        /**
         * *******************************************************************
         */
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuBar = new javax.swing.JMenuBar();
        menuPrincipal = new javax.swing.JMenu();
        menuPrincipal_Salir = new javax.swing.JMenuItem();
        menuCursos = new javax.swing.JMenu();
        menuCursos_Alta = new javax.swing.JMenuItem();
        menuCursos_Baja = new javax.swing.JMenuItem();
        menuCursos_Listado = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        menuPrincipal.setText("Principal");

        menuPrincipal_Salir.setText("Salir");
        menuPrincipal_Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPrincipal_SalirActionPerformed(evt);
            }
        });
        menuPrincipal.add(menuPrincipal_Salir);

        menuBar.add(menuPrincipal);

        menuCursos.setText("Cursos");

        menuCursos_Alta.setText("Alta");
        menuCursos_Alta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_optionActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Alta);

        menuCursos_Baja.setText("Baja");
        menuCursos_Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_optionActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Baja);

        menuCursos_Listado.setText("Listado");
        menuCursos_Listado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_optionActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Listado);

        menuBar.add(menuCursos);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 407, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 279, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuPrincipal_SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPrincipal_SalirActionPerformed
        int op = JOptionPane.showConfirmDialog(this,
                "¿Está seguro que desea cerrar la aplicación?",
                this.getTitle(),
                JOptionPane.YES_NO_OPTION);
        switch (op) {
            case JOptionPane.YES_OPTION:
                /* CERRAR CONEXIÓN BD *****************************************/
                try {
                    //System.out.println("Cerrando conexión BD...");
                    bd.cerrarConexion();
                    //System.out.println("Conexión cerrada correctamente.");
                    JOptionPane.showMessageDialog(this, "Conexion cerrada correctamente", "Base de datos", JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception e) {
                    //System.out.println("Error!!\n"+e.getMessage());
                    JOptionPane.showMessageDialog(this,
                            "Error!! " + e.getMessage(), "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                /**
                 * ***********************************************************
                 */
                this.setVisible(false);
                this.dispose();
                break;
        }
    }//GEN-LAST:event_menuPrincipal_SalirActionPerformed

    private void menuCursos_optionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCursos_optionActionPerformed
        panelCA.setVisible(false);
        panelCB.setVisible(false);
        panelCL.setVisible(false);
        if (evt.getSource() == menuCursos_Alta) {
            panelCA.setVisible(true);
            panelCB.setVisible(false);
            panelCL.setVisible(false);
        } else if (evt.getSource() == menuCursos_Baja) {
            panelCA.setVisible(false);
            panelCB.setVisible(true);
            panelCL.setVisible(false);
        } else if (evt.getSource() == menuCursos_Listado) {
            panelCA.setVisible(false);
            panelCB.setVisible(false);
            panelCL.setVisible(true);
            panelCL.listadoCurso(modeloLista);
        }
    }//GEN-LAST:event_menuCursos_optionActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new T12p01_GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuCursos;
    private javax.swing.JMenuItem menuCursos_Alta;
    private javax.swing.JMenuItem menuCursos_Baja;
    private javax.swing.JMenuItem menuCursos_Listado;
    private javax.swing.JMenu menuPrincipal;
    private javax.swing.JMenuItem menuPrincipal_Salir;
    // End of variables declaration//GEN-END:variables
}
